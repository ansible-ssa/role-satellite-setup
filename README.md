# README

- Channels sync can take a long time - watch your Satellite UI to see what's going on, either open **Monitor** -> **Tasks** or **Content** -> **Sync Status**
- these are not async jobs since the next tasks depend on the initial sync to complete
- depending on your internet connection the first playbook run can take very long (hours?)

## Variables

satellite_skip_check: boolean (default: false) - if true it will start the satellite-installer with parameter `-s` which will skip all internal checks, this can be useful if the environment doesn't have proper DNS setup. It is still recommended to add the necessary records to `/etc/hosts´ or the installer might fail.
satellite_admin_password: (default: empty) - the password which is used when creating the Satellite admin account, can not be empty.
